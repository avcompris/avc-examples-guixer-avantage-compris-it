# avc-examples-guixer-avantage-compris-it

## Description

An example of usage of the `guixer-core` testing library.

## Files in this repository

  * src/main/resources/
     * buildinfo.properties
  * src/test/java/
     * net/avcompris/examples/avantage-compris/it/
         * HomeTest.java
  * src/test/resources
     * env.properties
  * .gitlab-ci/
     * settings.xml
  * .gitlab-ci.yml
  * pom.xml
  * README.md