package net.avcompris.examples.avantage_compris.it;

import org.junit.jupiter.api.Test;

import net.avcompris.guixer.core.AbstractUITest;
import net.avcompris.guixer.core.UIEnv;

@UIEnv(baseURL = "https://www.avantage-compris.com/")
public class HomeTest extends AbstractUITest {

	@Test
	public void testHome() throws Exception {

		get()

				.waitFor("#body-home")

				.takeScreenshot("home");
	}
}
